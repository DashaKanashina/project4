package ru.unn.Project4;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
public class Main {
    public static void main(String[] args) {
        String dirName = "C://SE2020_LESSON9";
        File dir = new File(dirName);
        boolean create = dir.mkdir();
        OutputStream output;
        int  numberDir = (int) (Math.random() * 3 + 1);
        for (int i = 0; i <= numberDir; i++) {
            if (i != 0) {
                dirName = dirName.concat("//Directory_".concat(Integer.toString(i)));
            }
            dir = new File(dirName);
            boolean create1 = dir.mkdir();
            int numberFile = (int) (Math.random() * 3 + 1);
            for (int j = 1; j <= numberFile; j++) {
                try {
                    output = new FileOutputStream(dirName.concat("//File_" + Integer.toString(j) + ".txt"));
                    int numberSymbol = (int) (Math.random() * 191 + 10);
                    for (int j2 = 1; j2 <= numberSymbol; j2++) {
                        int randomNumber = (int) (Math.random() * 10);
                        char randomChar = (char) (randomNumber + '0');
                        output.write(randomChar);
                        output.write(' ');
                    }
                    output.close();
                } catch (IOException e) {
                    System.out.println("Ошибка");
                }
            }
        }
    }
}
